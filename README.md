## Hello there! 👋
I am a Programmer that likes to make open source projects and test random stuff!

## 🔭 I’m currently working on
I am a student, so I don't have any major projects, but in general, something random that I found on the internet and wanted to checkout

## 🌱 I’m currently learning 
- Rust
- C++ / C, GO, Python 
- CSS, HTML
- Javascript / Typescript
- llvm

## 👯 I’m looking to collaborate on
- Open Source Cross Platform applications and libraries

## 🤔 I’m helping with
- [bgfx.rs](https://github.com/lmtr0/bgfx-rs)
- [The Higenku Project](https://higenku.org)

## 📘 Author of
- [Rocket.Svelte.GraphQL Template](https://github.com/lmtr0/routify-rocket.rs-template)
- For CAS, a [the OngSearch](https://gitlab.com/lmtr0/ongsearch)
- The Cross Platform App Platform: [Higenku](https://higenku.org)

## 💬 Ask me about
- Fun libraries to work with
- C#, C++, C, Rust, Javascript, Typescript and Python
- Mongodb, CouchDb
- Linux in general (except ubuntu) 
- containers

## 📫 How to reach me
- Send me an email at <lmtr0@protonmail.ch>
- Send me an email at <lmtr0@outlook.com>
